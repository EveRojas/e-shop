export default{
	MONGODB_URL: process.env.MONGODB_URL || 'mongodb://localhost/e-shop-project',
	JWT_SECRET: process.env.JWT_SECRET || 'segredo'
}