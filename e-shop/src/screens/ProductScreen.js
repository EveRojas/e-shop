import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsProduct, listProducts } from '../actions/productActions';
//import data from '../data';

function ProductScreen(props) {
  const [qty, setQty] = useState(1);
  const productDetails = useSelector(state => state.productDetails);
  const { product, loading, error } = productDetails;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(detailsProduct(props.match.params.id));
    return ()=> {
    //
    };
  }, []);

  const handleAddtoCart = () => {
    props.history.push("/cart/" + props.match.params.id + "?qty=" + qty)
  }

	// console.log(props.match.params.id);
	// const product= data.products.find(x => x._id === props.match.params.id)
	return <div>
          	<div className="back-to-result">
          	  <Link to="/"> Voltar </Link>
            </div>
            {loading ? <div>Loading...</div> :
              error ? <div>{error}</div> :
              (
              <div className='details'>
               <div className='details-image'>
                 <img src={product.image} alt="product"></img>
               </div>
              <div className='details-info'>
              <ul>
                <li key={product._id}>
                   <h4>{product.title}</h4>
                </li>
                <li key={product._id}>
                   {product.rating} estrelas ({product.numReviews} Avaliações)
                </li>
                <li key={product._id}>
                 <b>Detalhes do produto:</b>
                 <div>
                 {product.description}
                 </div>
                </li>
              </ul>
            </div>
            <div className="details-action">
              <ul>
                <li key={product._id}>
                Preço: R${product.price}
                </li>
                <li key={product._id}>
                Status: {product.countInStock > 0 ? "Disponível" : "Indisponível" }
                </li>
                <li key={product._id}>
                Quantidade:<select value={qty} 
                                   onChange={(e) => {setQty(e.target.value)}}
                                   className="select">
                           {[...Array(product.countInStock).keys()].map(x=>
                           <option key={x + 1} value={x + 1}>{x + 1}</option> 
                           )}
                 </select>
                </li>
                <li key={product._id}>
                  {product.countInStock > 0 && <button onClick={handleAddtoCart} className="button primary">Adicionar ao Carrinho</button>
                  }
                </li>
              </ul>
          </div>
    </div>
     )
    }
</div>
}

export default ProductScreen;