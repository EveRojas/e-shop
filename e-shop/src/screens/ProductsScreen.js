import React, { useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { signin } from '../actions/userActions';
import { deleteProduct, saveProduct, listProducts } from '../actions/productActions';
//import data from '../data';

function ProductsScreen(props) {
  
  const [modalVisible, setModalVisible] = useState(false);
  const [id, setId] = useState('');
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [brand, setBrand] = useState('');
  const [category, setCategory] = useState('');
  const [countInStock, setCountInStock] = useState('');
  const [description, setDescription] = useState('');
  
  const productList = useSelector(state => state.productList); 
  const {loading, products, error} = productList; 
  
  const productDelete = useSelector(state => state.productDelete);
  const {loading: loadingDelete, success: successDelete, error: errorDelete} = productDelete;
  
  const productSave = useSelector(state => state.productSave);
  const {loading: loadingSave, success: successSave, error: errorSave} = productSave;
  
  const dispatch = useDispatch();

  useEffect(() => {
    if(successSave){
      setModalVisible(false);
    }
    dispatch(listProducts());
    return ()=> {
    //
    };
  }, [successSave, successDelete]);

const openModal =(product)=>{
  setModalVisible(true);
  setId(product._id);
  setPrice(product.price);
  setImage(product.image);
  setBrand(product.brand);
  setCategory(product.category);
  setCountInStock(product.countInStock);
  setDescription(product.description);

}

const submitHandler = (e) => {
	e.preventDefault();
	dispatch(saveProduct({_id:id, name, price, image, brand, category, countInStock, description }));
}

const deleteHandler=(product) =>{
  dispatch(deleteProduct(product._id))
}
	// console.log(props.match.params.id);
	// const User= data.products.find(x => x._id === props.match.params.id)
	return <div className="content content-margined">

            <div className="product-header">
                   <h3>Products</h3>
                   <button className="button primary" onClick={()=> openModal({})}> Create Product </button>
            </div>
         {modalVisible &&   
            <div className="form">
              <form onSubmit={submitHandler}>
                <ul className="form-container">
                 <li>
                 <h2>Criar Produto</h2>
                 </li>
                     <li>
                      {loadingSave && <div> Loading... </div>}
                      {errorSave && <div>{errorSave}</div>}
                     </li>
                 <li>
                 <label htmlFor="name">
                 Name                </label>
                  <input type="name" name="name" value={name} id="name" onChange={(e)=>setName(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="price">
                 Price                </label>
                  <input type="price" name="price"  value={price} id="price" onChange={(e)=>setPrice(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="image">
                 Image                </label>
                  <input type="image" name="image"  value={image} id="image" onChange={(e)=>setImage(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="brand">
                 Brand                </label>
                  <input type="brand" name="brand"  value={brand} id="brand" onChange={(e)=>setBrand(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="category">
                 Category                </label>
                  <input type="category" name="category"  value={category} id="category" onChange={(e)=>setCategory(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="countInStock">
                 Count in Stock                </label>
                  <input type="countInStock" name="countInStock" value={countInStock} id="countInStock" onChange={(e)=>setCountInStock(e.target.value)}></input>
                 </li>

                 <li>
                 <label htmlFor="description">
                 Description                </label>
                  <textarea name="description"  value={description} id="description" onChange={(e)=>setDescription(e.target.value)}></textarea>
                 </li>

                 <li>
                  <button type="submit" className="button primary">{id? "Update" : "Criar" }</button>
                  <button type="button" onClick={()=>setModalVisible(false)} className="button secondary"> Voltar </button>
               
                 </li>
                 
                </ul>
             </form>
           </div>  
         }
          
           <div className="product-list">
                    <table className="table">
                      <thead>
                       <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th> 
                        <th>Brand</th>
                        <th>Category</th>
                        <th>Action</th>
                       </tr>
                      </thead>
                      <tbody>
                      { products.map(product=>(<tr key={product._id}>
                        <td>{product._id}</td>
                        <td>{product.name}</td>
                        <td>{product.price}</td>
                        <td>{product.brand}</td> 
                        <td>{product.category}</td>
                        <td>
                            <button className="button" onClick={()=>openModal(product)}> Edit </button>
                            {' '} 
                            <button className="button" onClick={()=>deleteHandler(product)}> Delete </button>
                        </td>
                       </tr>))}                         
                      </tbody>
                    </table>
          </div>
        </div>
}

export default ProductsScreen;
