import React, {useEffect} from 'react';
// import axios from 'axios';
import store from '../store';
import {Link} from 'react-router-dom';
import { useSelector, useDispatch} from 'react-redux';
import { listProducts } from '../actions/productActions';
// import data from '../data.js';
// import util from '../util.js'; 

function HomeScreen(props) {

  // const [products, setProduct]=useState([]);
  const productList = useSelector(state => state.productList);
  const { products, loading, error} = productList;
  const dispatch = useDispatch();
	useEffect(()=> {
       dispatch(listProducts());
       // const fetchData = async () => {
       // 	 const {data} = await axios.get("/api/products");
       // 	 setProduct(data);
       // } 
       //  fetchData();
       return () =>{
                // 
      };
	}, [])
	
	return loading ? <div>Loading...</div> :
    error ? <div>{error}</div> : 
  <div className="content">
      <ul className="products">
         {
         	// data.products statically
         	products.map(product => (
                <li key={products._id}>
                  <div className="product">
                   <Link to={'/product/' + product._id}>
                     <img className="product-image" src={product.image} alt={product.title}></img>
                     </Link>
                 
    		          <div className="product-title">
		                 <Link to={'/product/' + product._id}>{product.title}</Link>
                      </div>
                    <div className="product-brand">{product.brand}</div>  
                    <div className="product-rating">{product.rating} estrelas ({product.numReviews} avaliações) </div>
                    <div className="product-price">R${product.price}</div>
                  </div> 
                </li>))
         }
      </ul>
    </div>
}
export default HomeScreen;