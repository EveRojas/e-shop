import { render } from 'react-dom'
import React, { useState, useEffect } from 'react'
import { useTransition, animated, config } from 'react-spring'

const slides = [

  { id: 0, url: 'photo-1558190596-69daf12cf424?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9=375&q=80' },
  { id: 1, url: 'photo-1569956726914-a9db65466c5b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9=750&q=80' },
  { id: 2, url: 'photo-1579621970563-ebec7560ff3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9=751&q=80' },
]

const AboutCarousel = () => {
  const [index, set] = useState(0)
  const transitions = useTransition(slides[index], item => item.id, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: config.molasses,
  })  
  useEffect(() => void setInterval(() => set(state => (state + 1) % 3), 6000), [])
  return transitions.map(({ item, props, key }) => (
        <animated.div
          key={key}
          className="bg"
          style={{ ...props, backgroundImage: `url(https://images.unsplash.com/${item.url}&auto=format&fit=crop)` }}
        />
  ))
    
}



export default AboutCarousel;
