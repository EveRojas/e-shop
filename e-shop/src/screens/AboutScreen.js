import { render } from 'react-dom'
import React, { useState, useEffect } from 'react'
import { useTransition, animated, config } from 'react-spring'
import AboutCarousel from './AboutCarousel';
import AboutCall from './AboutCall';

const AboutScreen = (props) => {
  

  return <div className="about-content">
      <div className="about">  
        <div>
        <AboutCarousel/>
        </div>
      </div>
    </div> 
}



export default AboutScreen;


