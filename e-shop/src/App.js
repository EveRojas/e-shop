import React from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
// import data from "./data.js";
// import Products from "./screens/Products";
import ProductScreen from './screens/ProductScreen';
import HomeScreen from './screens/HomeScreen';
import AboutScreen from './screens/AboutScreen';
import CartScreen from './screens/CartScreen';
import SigninScreen from './screens/SigninScreen';
import RegisterScreen from './screens/RegisterScreen';
import ProductsScreen from './screens/ProductsScreen';
import {Logo} from './logo_market_whole.png';
import {SmallLogo} from './logo_market.png';
import { useSelector, useDispatch } from 'react-redux';

function App(){
 
 const userSignin = useSelector(state=> state.userSignin)
 const {userInfo} = userSignin;

 const openMenu=() =>{
  document.querySelector(".sidebar").classList.add("open");
 }
 
 const closeMenu= () => {
  document.querySelector(".sidebar").classList.remove("open")
 }
  
  return (
  <BrowserRouter>
    <div className="grid-container">
      <header className="header">
        <div className="brand">
          <button className="sidebar-open-button" onClick={openMenu}>
            &#9776;
          </button>
          <Link to="/"><img style={{width:'60%', alignItens:'center', justifyContent:'space-between', paddingTop:'0.5rem'}} src={require('./logo_market_whole.png')} alt="Logo"/>
          </Link>
        </div>
        <div className="header-links">
          <Link to="/cart/">Carrinho</Link>
         {
          userInfo? <Link to="/profile">{userInfo.name}</Link>:
                    <Link to="/signin">Entrar</Link>
         }
     
        </div>
      </header>
      <aside className="sidebar">
        <h3> Selecionar Menu: </h3>
          <button className="sidebar-close-button" onClick={closeMenu}>x</button> 
          <ul>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Café da Manhã </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/>  Vegetariano </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Frutos do Mar </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Carnes </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Sobremesas </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Sucos e Polpas </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/>  Vinhos </Link>
             </li>
          </ul>
          <h3> Saiba mais: </h3>
          <ul>
             <li>
                <Link to="/about" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Missão </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Ações </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Parcerias </Link>
             </li>
             <li>
                <Link to="/" style={{color:'white'}}><img style={{width:'8%', alignItens:'center', justifyContent:'space-between', paddingTop:'1rem'}} src={require('./logo_market.png')} alt="Small logo"/> Fala com a gente! </Link>
             </li>
          </ul>
      </aside>
      <main className="main">
        <div className="content">
          <Route path="/" exact={true} component={HomeScreen}/>
          <Route path="/about" component={AboutScreen}/>
          <Route path="/signin" component={SigninScreen}/>
          <Route path="/register" component={RegisterScreen}/>
          <Route path="/products" component={ProductsScreen}/>
          <Route path="/product/:id" component={ProductScreen}/>
          <Route path="/cart/:id?" component={CartScreen}/>
        </div>  
      </main>
      <footer className="footer">
      Mercado Consciente © 2020 ‎
      </footer>
    </div>
  </BrowserRouter>  
 );
}

export default App;
