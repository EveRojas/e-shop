const data={

	"products":[
     {
     	"_id":"dish1",
     	"title":"NHOQUE DE BATADA DOCE",
          "brand":"Bruna Grimaldi",
     	"image":"/images/dish1.jpg",
          "category":"Vegetariano",
     	"description": "Ao molho de gorgonzola, com beterraba, nozes e rúcula",
     	"price":28.70,
          "status":"Disponível",
     	"availableSizes":["for 1", "for 2", "Family size (4)"],
          "rating":5.0,
          "numReviews": 10, 

     },

     {
     	"_id":"dish2",
     	"title":"MELANCIA GRELHADA",
          "brand":"Bruna Grimaldi",
     	"image":"/images/dish2.jpg",
          "category":"Café da Manhã",
     	"description": "com hortelã, queijo coalho e mel",
     	"price": 10.20,
          "status":"Disponível",
     	"availableSizes":["for 1", "for 2", "Family size (4)"], 
          "rating":5.0,
          "numReviews": 10,
     },

     {
     	"_id":"dish3",
     	"title":"SALADA DE QUINOA",
          "brand":"Bruna Grimaldi",
     	"image":"/images/dish3.jpg",
          "category": "Frutos do Mar",
     	"description": "Ao molho pesto com camarões e crocante de parmesão",
     	"price": 32.50,
          "status":"Disponível",
     	"availableSizes":["for 1", "for 2", "Family size (4)"],
          "rating":5.0,
          "numReviews": 10,
      
     },

     {
     	"_id":"dish4",
     	"title":"SELEÇÃO DE VINHOS",
          "brand":"Winers",
     	"image":"/images/dish4.jpg",
          "category":"Vinhos",
     	"description": "Vinho harmonizado com a escolha do seu prato",
     	"price": 43.90,
          "status":"Disponível",
     	"availableSizes":["for 1", "for 2", "Family size (4)"],
          "rating":5.0,
          "numReviews": 10,
      
     } 
   ]
 }
 
export default data;